# Collaboration and Competition #

In this environment, two agents control rackets to bounce a ball over a net. If an agent hits the ball over the net, it receives a reward of +0.1. If an agent lets a ball hit the ground or hits the ball out of bounds, it receives a reward of -0.01. Thus, the goal of each agent is to keep the ball in play.

The observation space consists of 8 variables corresponding to the position and velocity of the ball and racket. Each agent receives its own, local observation. Two continuous actions are available, corresponding to movement toward (or away from) the net, and jumping.

The task is episodic, and in order to solve the environment, your agents must get an average score of +0.5 (over 100 consecutive episodes, after taking the maximum over both agents). Specifically,

- After each episode, we add up the rewards that each agent received (without discounting), to get a score for each agent. This yields 2 (potentially different) scores. We then take the maximum of these 2 scores.

- This yields a single score for each episode.

The environment is considered solved, when the average (over 100 episodes) of those scores is at least +0.5.

For this project, we use [MADDPG](https://papers.nips.cc/paper/7217-multi-agent-actor-critic-for-mixed-cooperative-competitive-environments.pdf) (Multiagent Deep Deterministic Policy Gradient) to solve the playing tennis task.

![Tennis](image/tennis.png)

## Library ##
1. python 3.6.8
2. gym 0.17.2
3. torch 1.6.0
4. matplotlib 3.3.1
5. numpy 1.19.1
7. unityagents 0.4.0

For more details, check `requirement.txt`

### File Structure ###
```
- model.py # the neural network model as function approximator
- buffer.py # replay buffer
- ddpg.py # DDPG algorithm code
- maddpg.py # Multiagent DDPG code
- Tennis-MADDPG.ipynb # notebook to train MADDPG agents
- checkpoint_actor_ma0.pth # weights of trained actor neural network of first agents in MADDPG setting
- checkpoint_actor_ma1.pth # weights of trained actor neural network of second agents in MADDPG setting
- checkpoint_critic_ma0.pth # weights of trained critic neural network of first agents in MADDPG setting
- checkpoint_critic_ma1.pth # weights of trained critic neural network of second agents in MADDPG setting
- README.md
```

### Instruction ###
For this project, we will not need to install Unity - this is because we can download the ready-to-use environment from one of the links below. You need only select the environment that matches your operating system:

- Linux: click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P3/Tennis/Tennis_Linux.zip)
- Mac OSX: click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P3/Tennis/Tennis.app.zip)
- Windows (32-bit): click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P3/Tennis/Tennis_Windows_x86.zip)
- Windows (64-bit): click [here](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P3/Tennis/Tennis_Windows_x86_64.zip)

Then, place the files in this repository.

After that, you can open  `Tennis-MADDPG.ipynb` to train MADDPG agents.

If you would like to change the neural network model, you can change `model.py`. And if you want to improve the algorithm, you can modify `maddpg.py` or `ddpg.py`.

### Results  ###

The reward plot of MADDPG Agents.

![20 Agents](image/maddpg_plot.PNG) 

### Acknowledgement ###
1. [Udacity](https://www.udacity.com/)
2. [Unity](https://unity.com/)