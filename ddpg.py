import numpy as np
import random
import copy
from collections import namedtuple, deque

from model import Actor, Critic
from buffer import ReplayBuffer
from noise import OUNoise
from utilities import soft_update, hard_update

import torch
import torch.nn.functional as F
import torch.optim as optim

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class DDPGAgent():
    """Interacts with and learns from the environment."""
    
    def __init__(self, num_agents, state_size, action_size, random_seed, 
        batch_size=128, buffer_size=int(1e8), 
        lr_actor = 3e-3, lr_critic=1e-3, weight_decay=0, gamma = 0.99,
        eps_end=0, eps_decay=0.999, epsilon=1.0):
        """Initialize an Agent object.
        
        Params
        ======
            state_size (int): dimension of each state
            action_size (int): dimension of each action
            random_seed (int): random seed
        """
        self.state_size = state_size
        self.action_size = action_size
        self.seed = random.seed(random_seed)

        # Actor Network (w/ Target Network)
        self.actor_local = Actor(state_size, action_size, random_seed, fc1_units=64, fc2_units=32).to(device)#, fc1_units=32, fc2_units=16).to(device)#
        self.actor_target = Actor(state_size, action_size, random_seed, fc1_units=64, fc2_units=32).to(device)#, fc1_units=32, fc2_units=16).to(device)#).to(device)#, fc1_units=12, fc2_units=8).to(device)#
        self.actor_optimizer = optim.Adam(self.actor_local.parameters(), lr=lr_actor)

        # Critic Network (w/ Target Network)
        self.critic_local = Critic(state_size*num_agents, action_size*num_agents, random_seed, fcs1_units=64, fc2_units=32).to(device)#, fcs1_units=128, fc2_units=64).to(device)#).to(device)#
        self.critic_target = Critic(state_size*num_agents, action_size*num_agents, random_seed, fcs1_units=64, fc2_units=32).to(device)#, fcs1_units=128, fc2_units=64).to(device)##, fcs1_units=64, fc2_units=32).to(device)#).to(device)#, fcs1_units=128, fc2_units=64).to(device)#
        self.critic_optimizer = optim.Adam(self.critic_local.parameters(), lr=lr_critic, weight_decay=weight_decay)

        # initialize targets same as original networks
        hard_update(self.actor_target, self.actor_local)
        hard_update(self.critic_target, self.critic_local)

        # Noise process
        self.noise = OUNoise(action_size, random_seed, theta=0.17, sigma=0.24)
        self.seed = random.seed(random_seed)

        self.warm_up =  batch_size

        self.gamma = gamma
        self.update_counter = 0     
        self.eps_end = eps_end  
        self.eps_decay = eps_decay   
        self.epsilon = epsilon

    def act(self, state, add_noise=True):
        """Returns actions for given state as per current policy.
        Params
        ======
            state (tensor): observations of the environment
            add_noise (bool): whether add action noise
        """
        state = torch.from_numpy(state).float().to(device)
        self.actor_local.eval()
        with torch.no_grad():
            action = self.actor_local(state).cpu().data.numpy()
        self.actor_local.train()
        if add_noise and (random.random() <= self.epsilon):
            action += self.epsilon*self.noise.sample()    
             
        return np.clip(action, -1, 1)

    def reset_and_decay(self):
        """Reset noise and decay the epsilon."""
        self.noise.reset()
        if len(self.memory) > self.warm_up: # > WARM_UP:
            # decrease epsilon
            self.epsilon = max(self.eps_end, self.eps_decay*self.epsilon) 
