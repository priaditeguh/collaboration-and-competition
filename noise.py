import numpy as np
import random
import copy
from collections import namedtuple, deque

class OUNoise():
    """Ornstein-Uhlenbeck process.
    https://en.wikipedia.org/wiki/Ornstein%E2%80%93Uhlenbeck_process
    """

    def __init__(self, size, seed, mu=0., theta=0.15, sigma=0.2):
        """Initialize parameters and noise process.
        Params
        ======
            size (int): the dimension of array 
            seed (int): seed the generator.
            mu (float): the Vasicek model.
            theta (float): Ornstein–Uhlenbeck parameter.
            sigma (float): Ornstein–Uhlenbeck parameter.
        """
        self.mu = mu * np.ones(size)
        self.theta = theta
        self.sigma = sigma
        self.seed = random.seed(seed)
        self.reset()

    def reset(self):
        """Reset the internal state (= noise) to mean (mu)."""
        self.state = copy.copy(self.mu)

    def sample(self):
        """Update internal state and return it as a noise sample."""
        x = self.state
        # use uniform [-1,1] instead of [0,1]
        dx = self.theta * (self.mu - x) + self.sigma * np.array([random.uniform(-1,1) for i in range(len(x))])
        self.state = x + dx
        return self.state