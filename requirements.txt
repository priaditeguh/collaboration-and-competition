absl-py==0.10.0
argon2-cffi==20.1.0
astor==0.8.1
atomicwrites==1.4.0
attrs==20.1.0
backcall==0.2.0
bleach==1.5.0
certifi==2020.6.20
cffi==1.14.2
cloudpickle==1.3.0
colorama==0.4.3
cycler==0.10.0
decorator==4.4.2
defusedxml==0.6.0
docopt==0.6.2
entrypoints==0.3
future==0.18.2
gast==0.4.0
grpcio==1.11.0
gym==0.17.2
html5lib==0.9999999
importlib-metadata==1.7.0
iniconfig==1.0.1
ipykernel==5.3.4
ipython==7.16.1
ipython-genutils==0.2.0
ipywidgets==7.5.1
jedi==0.17.2
Jinja2==2.11.2
jsonschema==3.2.0
jupyter==1.0.0
jupyter-client==6.1.7
jupyter-console==6.2.0
jupyter-core==4.6.3
kiwisolver==1.2.0
Markdown==3.2.2
MarkupSafe==1.1.1
matplotlib==3.3.1
mistune==0.8.4
more-itertools==8.5.0
nbconvert==5.6.1
nbformat==5.0.7
notebook==6.1.3
numpy==1.19.1
packaging==20.4
pandocfilters==1.4.2
parso==0.7.1
pickleshare==0.7.5
Pillow==7.2.0
pluggy==0.13.1
prometheus-client==0.8.0
prompt-toolkit==3.0.7
protobuf==3.5.2
py==1.9.0
pycparser==2.20
pyglet==1.5.0
Pygments==2.6.1
pyparsing==2.4.7
pyrsistent==0.16.0
pytest==6.0.1
python-dateutil==2.8.1
pywin32==228
pywinpty==0.5.7
PyYAML==5.3.1
pyzmq==19.0.2
qtconsole==4.7.6
QtPy==1.9.0
scipy==1.5.2
Send2Trash==1.5.0
six==1.15.0
tensorboard==1.7.0
tensorflow==1.7.1
termcolor==1.1.0
terminado==0.8.3
testpath==0.4.4
toml==0.10.1
torch==1.6.0
torchvision==0.7.0
tornado==6.0.4
traitlets==4.3.3
unityagents==0.4.0
wcwidth==0.2.5
Werkzeug==1.0.1
widgetsnbextension==3.5.1
zipp==3.1.0
